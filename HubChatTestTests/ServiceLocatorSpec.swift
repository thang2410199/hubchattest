//
//  HubChatTestTests.swift
//  HubChatTestTests
//
//  Created by Ngo Quoc Thang on 12/16/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import XCTest
import Foundation
import Quick
import Nimble
import Swinject

@testable import HubChatTest

class ServiceLocatorSpec: QuickSpec {
  override func spec() {
    var container: ServiceLocator!
    beforeEach {
      container = ServiceLocator()
    }
    
    describe("Container") {
      it("resolves every type.") {
        expect(container.getByType(IUnauthorizedService.self))
          .notTo(beNil())
        
        expect(container.getByType(IStorageCacheService.self))
          .notTo(beNil())
        
        expect(container.getByType(CategoryViewController.self))
          .notTo(beNil())
        
        expect(container.getByType(SplashScreenViewController.self))
          .notTo(beNil())
        
        expect(container.getByType(ICategoryViewModel.self))
          .notTo(beNil())
      }
    }
  }
}
