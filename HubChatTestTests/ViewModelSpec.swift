//
//  ViewModelSpec.swift
//  HubChatTestTests
//
//  Created by Ngo Quoc Thang on 12/16/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import XCTest
import Foundation
import Quick
import Nimble
import Swinject
import HubChatTest

@testable import HubChatTest

class ViewModelSpec: QuickSpec {
  override func spec() {
    let container = ServiceLocator()
    
    describe("category view model") {
      it("works") {
        var viewModel = container.getByType(ICategoryViewModel.self)
        viewModel.categoryName = "photography"
        viewModel.getPosts(refresh: true, parameter: nil)
        viewModel.getCategoryInfo()
        viewModel.savePosts([Post(uuid: "hduaidw")])
        let loadedPosts = viewModel.loadPosts()
        expect(loadedPosts?.first?.uuid).to(equal("hduaidw"))
      }
    }
  }
}
