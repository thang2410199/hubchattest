//
//  HubChatTestTests.swift
//  HubChatTestTests
//
//  Created by Ngo Quoc Thang on 12/16/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import XCTest
import Foundation
import Quick
import Nimble
import Swinject
import HubChatTest

@testable import HubChatTest

class ServiceSpec: QuickSpec {
  override func spec() {
    let container = ServiceLocator()
    
    describe("storage service") {
      it("save and load correctly") {
        let storageService = container.getByType(IStorageCacheService.self)
        let object = PostFlag.init(locked: true)
        storageService.saveJson(object, key: "test_object")
        let loadedObject: PostFlag? = storageService.loadJson("test_object")
        expect(loadedObject).notTo(beNil())
        expect(loadedObject?.isLocked).to(beTrue())
      }
    }
    
    describe("api service and mapping") {
      it("make get category request") {
        let networkService = container.getByType(IUnauthorizedService.self)
        var category: HubChatTest.Category?
        networkService.getCategory("photography", error: { (reason) in
          
          }, completion: { (result) in
            category = result.category
        })
        
        
        expect(category).toEventuallyNot(beNil(), timeout: 60)
      }
    }
    
    describe("api service and mapping") {
      it("make get category posts request") {
        let networkService = container.getByType(IUnauthorizedService.self)
        var posts: [Post]?
        networkService.getPosts("photography", parameter: nil, error: { (reason) in
          
          }, completion: { (result) in
            posts = result.posts
        })
        
        expect(posts).toEventuallyNot(beNil(), timeout: 60)
      }
    }
  }
}
