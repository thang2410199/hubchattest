//
//  ICategoryViewModel.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/17/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import Foundation

public protocol ICategoryViewModel {
  
  var categoryName: String? {get set}
  
  var gettingPost: Bool {get set}
  
  func getPosts(refresh: Bool, parameter: [String: AnyObject]?)
  
  func getCategoryInfo()
  
  func savePosts(_ posts: [Post]?)
  
  func loadPosts() -> [Post]?
}

public protocol CategoryViewModelDelegate {
  
  func updatePost(_ reload: Bool, posts: [Post]?, total: Int)
  
  func updateCategoryInfo(_ category: Category?)
  
  func networkError(_ reason: ErrorReason)
}
