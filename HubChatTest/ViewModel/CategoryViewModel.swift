//
//  CategoryViewModel.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/17/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import Foundation

class CategoryViewModel: ICategoryViewModel {
  
  let unauthorizedService: IUnauthorizedService
  let storageService: IStorageCacheService
  
  var categoryName: String?
  
  var gettingPost: Bool = false
  
  init(unauthorizedService: IUnauthorizedService, storageService: IStorageCacheService) {
    self.unauthorizedService = unauthorizedService
    self.storageService = storageService
  }
  
  func getCategoryInfo() {
    guard let category = categoryName else {
      print("can not get category info without category name")
      return
    }
    unauthorizedService.getCategory(category, error: errorHandler) { (result) in
      Broadcaster.notify(CategoryViewModelDelegate.self, block: { (observer) in
        observer.updateCategoryInfo(result.category)
      })
    }
  }
  
  func getPosts(refresh: Bool, parameter: [String: AnyObject]?) {
    guard let category = categoryName else {
      print("can not get post without category name")
      return
    }
    gettingPost = true
    unauthorizedService.getPosts(category, parameter: parameter, error: errorHandler) { (result) in
      self.gettingPost = false
      let totalPosts = result.meta?.total ?? 0
      Broadcaster.notify(CategoryViewModelDelegate.self, block: { (observer) in
        observer.updatePost(refresh, posts: result.posts, total: totalPosts)
      })
    }
  }
  
  func savePosts(_ posts: [Post]?) {
    if let posts = posts {
      let storedObject = SavedPosts(posts: posts)
      self.storageService.saveJson(storedObject, key: "saved_posts")
      print("store posts")
    }
  }
  
  func loadPosts() -> [Post]? {
    let savedObject: SavedPosts? = self.storageService.loadJson("saved_posts")
    return savedObject?.posts
  }
  
  func errorHandler(_ reason: ErrorReason) -> Void {
    self.gettingPost = false
    Broadcaster.notify(CategoryViewModelDelegate.self) { (observer) in
      observer.networkError(reason)
    }
  }
}
