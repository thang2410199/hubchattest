//
//  AppGlobalDelegate.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/17/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import Foundation

@objc
protocol AppGlobalDelegate {
  @objc optional
  func connectionAvailable()
  
  @objc optional
  func connectionLost()
  
  @objc optional
  func appWillTerminate()
}
