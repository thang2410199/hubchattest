//
//  AppDelegate.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/16/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import UIKit
import SnapKit
import ReachabilitySwift

// Entry point to resolve dependency
let serviceLocator = ServiceLocator()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  
  var reachability: Reachability?
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    
    handleNetworkAvaibilityChanges()
    
    disableNavBarShadow() // Remove NavBar Shadow
    
    setEntryViewController(launchOptions)
    
    return true
  }
  
  func setEntryViewController(_ launchOption: [AnyHashable: Any]?) {
    let viewController: UIViewController = serviceLocator.getByType(SplashScreenViewController.self, identifier: nil);
    let window = UIWindow(frame: UIScreen.main.bounds)
    window.rootViewController = viewController
    window.makeKeyAndVisible()
    self.window = window
  }
  
  func handleNetworkAvaibilityChanges() {
    reachability = Reachability()
    // Listen to networ avaiability changes
    NotificationCenter.default.addObserver(forName: ReachabilityChangedNotification, object: nil, queue: nil, using:checkForReachability)
    do {
      try reachability?.startNotifier()
    } catch {
      print("Unable to start notifier")
    }
  }
  
  func disableNavBarShadow() {
    UINavigationBar.appearance().setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
    UINavigationBar.appearance().shadowImage = UIImage()
    UINavigationBar.appearance().isTranslucent = false
    UINavigationBar.appearance().clipsToBounds = false
    UINavigationBar.appearance().tintColor = AppColor.White
  }
  
  func checkForReachability(_ notification:Notification) {
    let networkReachability = notification.object as! Reachability
    let remoteHostStatus = networkReachability.currentReachabilityStatus
    
    if (remoteHostStatus == .notReachable) {
      print("[Connectivity] Not Reachable")
      Broadcaster.notify(AppGlobalDelegate.self) { observer in
        observer.connectionLost?()
      }
    }
    else if (remoteHostStatus == .reachableViaWiFi || remoteHostStatus == .reachableViaWWAN) {
      print("[Connectivity] Reachable via Wifi or via WWAN")
      Broadcaster.notify(AppGlobalDelegate.self) { observer in
        observer.connectionAvailable?()
      }
    }
  }
  
  func applicationWillTerminate(_ application: UIApplication) {
    // Store the posts as cached json to show it next time
    Broadcaster.notify(AppGlobalDelegate.self) { (observer) in
      observer.appWillTerminate?()
    }
  }
  
  
}

