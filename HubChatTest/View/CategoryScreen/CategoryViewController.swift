//
//  CategoryViewController.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/17/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import UIKit
import SnapKit
import TransitionTreasury
import DZNEmptyDataSet
import Kingfisher
import Preheat

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class CategoryViewController: UIViewController {
  
  var viewModel: ICategoryViewModel?
  var dataSource: CategoryPostDataSource<Post, PostCell>?
  
  // UI Element
  var refreshControl: UIRefreshControl?
  var tableView = UITableView()
  var categoryHeader = CategoryHeaderView()
  
  // For preload the cell
  var preheatController: Preheat.Controller<UITableView>!
  
  // For transition animation
  weak var modalDelegate: ModalViewControllerDelegate?
  
  init(viewModel: ICategoryViewModel) {
    super.init(nibName: nil, bundle: nil)
    self.viewModel = viewModel
  }
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func loadView() {
    super.loadView()
    
    setupNavigationBar()
    setupLayout()
    Broadcaster.register(CategoryViewModelDelegate.self, observer: self)
    Broadcaster.register(CategoryPostDataSourceDelegate.self, observer: self)
    Broadcaster.register(AppGlobalDelegate.self, observer: self)
    
    self.refreshControl?.endRefreshing()
    
    self.loadCachedData()
    self.onRefreshPulled()
  }
  
  func setupNavigationBar() {
    self.navigationController?.navigationBar.barTintColor = AppColor.Clear
    self.navigationController?.navigationBar.isTranslucent = true
    self.navigationController?.view.backgroundColor = AppColor.Clear
    self.navigationController?.navigationBar.barStyle = .black
  }
  
  func setupLayout() {
    self.refreshControl = AutoShowRefreshControl()
    if let refreshControl = self.refreshControl {
      self.tableView.addSubview(refreshControl)
      self.refreshControl?.addTarget(self, action: #selector(onRefreshPulled), for: .valueChanged)
    }
    
    self.automaticallyAdjustsScrollViewInsets = false
    self.tableView.backgroundColor = AppColor.Clear
    self.tableView.showsVerticalScrollIndicator = false
    self.tableView.contentInset = UIEdgeInsets(top: 130, left: 0, bottom: 0, right: 0)
    self.tableView.emptyDataSetSource = self
    self.tableView.emptyDataSetDelegate = self
    self.tableView.estimatedRowHeight = 300
    self.tableView.rowHeight = UITableViewAutomaticDimension
    self.tableView.separatorStyle = .none
    // To get rid of the seperator with empty data
    self.tableView.tableFooterView = UIView()
    
    categoryHeader.backgroundColor = AppColor.LightGray
    categoryHeader.clipsToBounds = true
    self.view.addSubview(categoryHeader)
    self.view.addSubview(tableView)
    
    // This is for ios < 10, in ios 10 Apple introduce prefetchDataSource
    preheatController = Preheat.Controller(view : self.tableView)
    preheatController.enabled = true
    preheatController.handler = { [weak self] in
      self?.preheatWindowChanged(addedIndexPaths: $0, removedIndexPaths: $1)
    }
    
    self.view.backgroundColor = AppColor.White
    
    self.dataSource = CategoryPostDataSource(tableView: self.tableView, sectionCount: 1)
    
    self.setupConstraint()
  }
  
  func setupConstraint() {
    categoryHeader.snp.makeConstraints { (make) in
      make.top.left.right.equalToSuperview()
      make.height.equalTo(150)
    }
    
    tableView.snp.makeConstraints { (make) in
      make.left.right.bottom.equalToSuperview()
      // for status bar
      make.top.equalToSuperview().offset(20)
    }
  }
  
  func onRefreshPulled() {
    // Reload data
    self.viewModel?.getPosts(refresh: true, parameter: nil)
    self.viewModel?.getCategoryInfo()
  }
  
  func loadCachedData() {
    let cachedPosts = self.viewModel?.loadPosts()
    if let cachedPosts = cachedPosts {
      if cachedPosts.count > 0 {
        self.dataSource?.add(cachedPosts)
        self.tableView.reloadData()
      }
    }
  }
  
  // Preheat
  func preheatWindowChanged(addedIndexPaths added: [IndexPath], removedIndexPaths removed: [IndexPath]) {
    var urls : [URL] = []
    for index in added {
      let post = self.dataSource?.objects[index.row]
      if let url = post?.entities?.images?.first?.cdnUrl?.toURL() {
        urls.append(url)
      }
      if let url = post?.creator?.avatar?.url?.toURL() {
        urls.append(url)
      }
      if let url = post?.preview?.content?.image?.toURL() {
        urls.append(url)
      }
    }
    
    let prefetcher = ImagePrefetcher(urls: urls) {
      skippedResources, failedResources, completedResources in
      print("These resources are skipped: \(skippedResources)")
      print("These resources are prefetched: \(completedResources)")
      print("These resources are failed to prefetch: \(failedResources)")
    }
    prefetcher.start()
  }
}

// MARK: Empty data placeholder
extension CategoryViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
  func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString {
    let text = "No post yet"
    let paragraph = NSMutableParagraphStyle()
    paragraph.lineBreakMode = .byWordWrapping
    paragraph.alignment = .center
    let attributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 14.0), NSForegroundColorAttributeName: UIColor.lightGray, NSParagraphStyleAttributeName: paragraph]
    return NSAttributedString(string: text, attributes: attributes)
  }
  
  func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
    return true
  }
}

// MARK: Listen to view model
extension CategoryViewController: CategoryViewModelDelegate {
  func updateCategoryInfo(_ category: Category?) {
    categoryHeader.configure(category)
  }
  
  func updatePost(_ reload: Bool, posts: [Post]?, total: Int) {
    self.refreshControl?.endRefreshing()
    self.dataSource?.updateTotal(total)
    if let posts = posts {
      
      // API pagination return duplicated data
      let filteredPost = posts.filter({ (filterPost) -> Bool in
        self.dataSource?.objects.filter({ (post) -> Bool in
          post.uuid == filterPost.uuid
        }).count == 0
      })
      
      
      if (reload) {
        self.dataSource?.clear()
        self.dataSource?.add(filteredPost)
        self.tableView.reloadData()
      }
      else {
        self.dataSource?.addAndUpdate(filteredPost, section: 0)
      }
    }
  }
  
  func networkError(_ reason: ErrorReason) {
    if(self.presentedViewController == nil) {
      self.showAlert("Error", message: "There is problem connecting to server, please try again later", complete: nil)
    }
    self.refreshControl?.endRefreshing()
  }
}

extension CategoryViewController: CategoryPostDataSourceDelegate {
  func didSelectCell(_ indexPath: IndexPath) {
    
  }
  
  func loadMoreDataTriggered() {
    let gettingData = self.viewModel?.gettingPost ?? false
    let hasMoreData = self.dataSource?.hasMoreData ?? false
    if(!gettingData && hasMoreData) {
      self.viewModel?.getPosts(refresh: false, parameter: self.getNextPageParameter())
    }
    
  }
  
  func getNextPageParameter() -> [String: AnyObject] {
    if(self.dataSource?.objects.count > 0) {
      if let lastTimeStamp = self.dataSource?.objects.last?.timestamp {
        return ["until": "\(lastTimeStamp)" as AnyObject]
      }
      else {
        return [:]
      }
    }
    else {
      return [:]
    }
  }
}

extension CategoryViewController: AppGlobalDelegate {
  
  func connectionAvailable() {
    runOnMainThread {
      self.showStatusBarNotification("Connection recovered", forSecond: 2, style: JDStatusBarStyle.Success, withActivity: false)
    }
  }
  
  func connectionLost() {
    runOnMainThread {
      self.showStatusBarNotification("Connection lost", forSecond: 2, style: JDStatusBarStyle.Warning, withActivity: false)
    }
  }
  
  func appWillTerminate() {
    var counter = 0
    let savedPosts = dataSource?.objects.filter({ (post) -> Bool in
      counter += 1
      return counter <= 10
    })
    self.viewModel?.savePosts(savedPosts)
  }
}
