//
//  PostCell.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/18/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import UIKit
import SnapKit
import Kingfisher

class PostCell: UITableViewCell, ConfigurableCell {
  
  static var identifier: String = "PostCell"
  static let AvatarSize: CGFloat = 30
  static let Margin: CGFloat = 8
  static let ElementMargin: CGFloat = 4
  static let PreviewThumbnailSize = CGSize(width: 120, height: 120)
  static let PreviewSiteNameHeight: CGFloat = 16
  
  var post: Post?
  
  // UI Element
  var usernameLabel = UILabel()
  var avatarImageView = UIImageView()
  var userView = UIView()
  var dateLabel = UILabel()

  // TODO: If the cell's layout is too complex, change to generate layout dynamicly (each cell has difference UI elements)
  var postTextLabel = UILabel()
  
  var postThumbnailImageView = UIImageView()
  var numberOfImagesBackground = UIView()
  var numberOfImagesLabel = UILabel()
  var postView = UIView()

  var likeIconView = UIImageView()
  var likeCountLabel = UILabel()
  var commentIconView = UIImageView()
  var commentCountLabel = UILabel()
  var moreOptionView = DotsView(numberOfDots: 3, color: AppColor.LightGray, dotRadius: 2)
  var postFooterView = UIView()
  
  var linkPreviewIndicator = UIView()
  var linkPreviewSitenameLabel = UILabel()
  var linkPreviewTitleLabel = UILabel()
  var linkPreviewDescriptionLabel = UILabel()
  var linkPreviewThumbnail = UIImageView()
  
  func configure(for object: Post, at indexPath: IndexPath) {
    self.post = object
    
    self.usernameLabel.text = object.creator?.displayName
    self.avatarImageView.kf.setImage(with: object.creator?.avatar?.url?.toURL(), placeholder: ImagePlaceholder.UserAvatar, options: [.transition(.fade(0.2)), .processor(ResizingImageProcessor(targetSize: CGSize(width: 100, height: 100)))]) {
      (image, error, cacheType, imageUrl) in
      self.setImageForView(view: self.avatarImageView, image: image, error: error, cacheType: cacheType, imageUrl: imageUrl)
    }

    // Image posts
    if let postThumbnail = object.entities?.images?.first {
      // I would like to change it so the cell's height is caculated before changing any constraint
      // Update constraint with autolayout for every cell at run time is costly.
      // It's solely because of the requirement of the test to use only autolayout
      if let thumbnailWidth = postThumbnail.width, let thumbnailHeight = postThumbnail.height {
        postView.snp.updateConstraints({ (make) in
          make.height.equalTo(self.frame.size.width * thumbnailHeight.toCGFloat() / thumbnailWidth.toCGFloat()).priority(999)
        })
      }
      
      postThumbnailImageView.kf.setImage(with: postThumbnail.cdnUrl?.toURL(), options: [.transition(.fade(0.2)), .processor(ResizingImageProcessor(targetSize: CGSize(width: 400, height: 400)))]) {
        (image, error, cacheType, imageUrl) in
        self.setImageForView(view: self.postThumbnailImageView, image: image, error: error, cacheType: cacheType, imageUrl: imageUrl)
      }
    } else {
      // Post with empty images and links
      if(object.entities?.images?.count == 0 && object.entities?.links?.count == 0) {
        postView.snp.updateConstraints({ (make) in
          make.height.equalTo(0).priority(999)
        })
      }
    }

    if let previewLink = object.preview?.link {
      postTextLabel.text = object.rawContent?.replace(previewLink, withString: "")
    }
    else {
      postTextLabel.text = object.rawContent
    }

    // Linking post
    if let previewContent = object.preview?.content {

      // Linking post with only image
      if(previewContent.description == nil && previewContent.title == nil) {
        if let thumbnailWidth = previewContent.imageInfo?.width, let thumbnailHeight = previewContent.imageInfo?.height {
          postView.snp.updateConstraints({ (make) in
            make.height.equalTo(self.frame.size.width * thumbnailHeight.toCGFloat() / thumbnailWidth.toCGFloat()).priority(999)
          })
        }
        
        postThumbnailImageView.kf.setImage(with: previewContent.image?.toURL(), options: [.transition(.fade(0.2)), .processor(ResizingImageProcessor(targetSize: CGSize(width: 400, height: 400)))]) {
          (image, error, cacheType, imageUrl) in
          self.setImageForView(view: self.postThumbnailImageView, image: image, error: error, cacheType: cacheType, imageUrl: imageUrl)
        }
      }
      // Post with preview link
      else {
        linkPreviewIndicator.isHidden = false
        linkPreviewSitenameLabel.text = previewContent.site?.name
        linkPreviewTitleLabel.text = previewContent.title
        linkPreviewDescriptionLabel.text = previewContent.description
        if let previewImageUrl = previewContent.image?.toURL() {
          linkPreviewThumbnail.snp.updateConstraints({ (make) in
            make.width.equalTo(PostCell.PreviewThumbnailSize.width)
          })

          linkPreviewThumbnail.kf.setImage(with: previewContent.image?.toURL(), options: [.transition(.fade(0.2)), .processor(ResizingImageProcessor(targetSize: CGSize(width: 400, height: 400)))]) {
            (image, error, cacheType, imageUrl) in
            self.setImageForView(view: self.linkPreviewThumbnail, image: image, error: error, cacheType: cacheType, imageUrl: imageUrl)
          }
        }
        else {
          linkPreviewThumbnail.snp.updateConstraints({ (make) in
            make.width.equalTo(0)
          })
        }
        if(object.entities?.images?.first == nil) {
          postView.snp.updateConstraints({ (make) in
            make.height.equalTo(PostCell.PreviewSiteNameHeight + PostCell.ElementMargin * 2 + PostCell.PreviewThumbnailSize.height + 2).priority(999)
          })
        }
      }
    }

    dateLabel.text = object.createdDate?.timeAgoSimple
    likeCountLabel.text = object.stats?.upVotes?.toString()
    commentCountLabel.text = object.cache?.totalComment?.toString()
  }
  
  func loadView() {
    self.selectionStyle = .none

    userView.addSubview(usernameLabel)
    userView.addSubview(avatarImageView)
    userView.addSubview(dateLabel)

    linkPreviewIndicator.addSubview(linkPreviewSitenameLabel)
    linkPreviewIndicator.addSubview(linkPreviewTitleLabel)
    linkPreviewIndicator.addSubview(linkPreviewDescriptionLabel)
    linkPreviewIndicator.addSubview(linkPreviewThumbnail)

    postView.addSubview(postThumbnailImageView)
    postView.addSubview(linkPreviewIndicator)

    postFooterView.addSubview(likeIconView)
    postFooterView.addSubview(likeCountLabel)
    postFooterView.addSubview(commentIconView)
    postFooterView.addSubview(commentCountLabel)
    postFooterView.addSubview(moreOptionView)
    
    contentView.addSubview(userView)
    contentView.addSubview(postTextLabel)
    contentView.addSubview(postView)
    contentView.addSubview(postFooterView)
    
    contentView.autoresizingMask = UIViewAutoresizing.flexibleHeight
    
    avatarImageView.setCornerRadius(PostCell.AvatarSize / 2)
    avatarImageView.layer.borderColor = AppColor.MainColor.cgColor
    avatarImageView.layer.borderWidth = 1
    
    usernameLabel.textColor = AppColor.LightGray

    dateLabel.textColor = AppColor.LightGray
    dateLabel.font = FontBook.systemFont(.Regular, size: 12)

    postTextLabel.lineBreakMode = .byWordWrapping
    postTextLabel.numberOfLines = 0

    linkPreviewIndicator.layer.borderWidth = 1
    linkPreviewIndicator.isHidden = true
    // Careful with this
    linkPreviewIndicator.setCornerRadius(PostCell.ElementMargin)
    linkPreviewIndicator.layer.borderColor = AppColor.LightGray.cgColor

    linkPreviewSitenameLabel.textColor = AppColor.LightGray
    linkPreviewSitenameLabel.font = FontBook.systemFont(.Regular, size: 12)

    linkPreviewTitleLabel.font = FontBook.systemFont(.Bold, size: 12)
    linkPreviewTitleLabel.numberOfLines = 2
    linkPreviewTitleLabel.lineBreakMode = .byTruncatingTail

    linkPreviewDescriptionLabel.numberOfLines = 3
    linkPreviewDescriptionLabel.lineBreakMode = .byTruncatingTail

    linkPreviewThumbnail.setCornerRadius(PostCell.ElementMargin)
    linkPreviewThumbnail.contentMode = .scaleAspectFill

    likeIconView.backgroundColor = AppColor.MainColor
    likeCountLabel.textColor = AppColor.LightGray
    likeCountLabel.font = FontBook.systemFont(.Regular, size: 12)

    commentIconView.backgroundColor = AppColor.LightGray
    commentCountLabel.textColor = AppColor.LightGray
    commentCountLabel.font = FontBook.systemFont(.Regular, size: 12)

    moreOptionView.delegate = self
    
    userView.snp.makeConstraints { (make) in
      make.height.equalTo(30 + 8 * 2).priority(999)
      make.top.left.right.equalToSuperview()
    }
    
    avatarImageView.snp.makeConstraints { (make) in
      make.top.left.equalToSuperview().offset(PostCell.Margin)
      make.size.equalTo(CGSize(width: PostCell.AvatarSize, height: PostCell.AvatarSize))
    }
    
    usernameLabel.snp.makeConstraints { (make) in
      make.left.equalTo(avatarImageView.snp.right).offset(PostCell.Margin)
      make.centerY.equalToSuperview()
    }

    dateLabel.snp.makeConstraints { (make) in
      make.right.equalToSuperview().offset(-PostCell.Margin)
      make.centerY.equalToSuperview()
    }

    postTextLabel.snp.makeConstraints { (make) in
      make.top.equalTo(userView.snp.bottom)
      make.left.equalToSuperview().offset(PostCell.Margin)
      make.right.equalToSuperview().offset(-PostCell.Margin)
    }
    
    postView.snp.makeConstraints { (make) in
      make.top.equalTo(postTextLabel.snp.bottom).offset(PostCell.Margin)
      make.left.right.equalToSuperview()
      make.height.equalTo(PostCell.PreviewSiteNameHeight + PostCell.ElementMargin * 2 + PostCell.PreviewThumbnailSize.height + 2).priority(999)
      make.bottom.greaterThanOrEqualTo(linkPreviewDescriptionLabel.snp.bottom)
    }

    linkPreviewIndicator.snp.makeConstraints { (make) in
      make.top.left.equalToSuperview().offset(PostCell.Margin)
      make.right.bottom.equalToSuperview().offset(-PostCell.Margin)
    }

    linkPreviewSitenameLabel.snp.makeConstraints { (make) in
      make.top.left.equalToSuperview().offset(PostCell.ElementMargin)
      make.height.equalTo(PostCell.PreviewSiteNameHeight)
    }

    linkPreviewTitleLabel.snp.makeConstraints { (make) in
      make.left.equalToSuperview().offset(PostCell.ElementMargin)
      make.right.equalTo(linkPreviewThumbnail.snp.left).offset(-PostCell.ElementMargin)
      make.top.equalTo(linkPreviewSitenameLabel.snp.bottom).offset(PostCell.ElementMargin)
    }

    linkPreviewDescriptionLabel.snp.makeConstraints { (make) in
      make.left.equalToSuperview().offset(PostCell.ElementMargin)
      make.right.equalTo(linkPreviewThumbnail.snp.left).offset(-PostCell.ElementMargin)
      make.top.equalTo(linkPreviewTitleLabel.snp.bottom).offset(PostCell.ElementMargin)
    }

    linkPreviewThumbnail.snp.makeConstraints { (make) in
      make.right.equalToSuperview()
      make.top.equalTo(linkPreviewSitenameLabel.snp.bottom).offset(PostCell.ElementMargin)
      make.size.equalTo(PostCell.PreviewThumbnailSize)
    }

    postFooterView.snp.makeConstraints { (make) in
      make.top.equalTo(postView.snp.bottom)
      make.left.right.equalToSuperview()
      make.bottom.equalToSuperview()
      make.height.equalTo(54).priority(999)
    }

    moreOptionView.snp.makeConstraints { (make) in
      make.centerY.equalToSuperview()
      make.right.equalToSuperview().offset(-PostCell.Margin)
      make.width.equalTo(30)
    }

    likeIconView.snp.makeConstraints { (make) in
      make.size.equalTo(CGSize(width: 30, height: 30))
      make.centerY.equalToSuperview()
      make.left.equalToSuperview().offset(PostCell.Margin)
    }

    likeCountLabel.snp.makeConstraints { (make) in
      make.centerY.equalToSuperview()
      make.left.equalTo(likeIconView.snp.right).offset(PostCell.ElementMargin)
      make.width.equalTo(50)
    }

    commentIconView.snp.makeConstraints { (make) in
      make.size.equalTo(CGSize(width: 30, height: 30))
      make.centerY.equalToSuperview()
      make.left.equalTo(likeCountLabel.snp.right).offset(PostCell.Margin)
    }

    commentCountLabel.snp.makeConstraints { (make) in
      make.centerY.equalToSuperview()
      make.left.equalTo(commentIconView.snp.right).offset(PostCell.ElementMargin)
      make.width.equalTo(50)
    }
    
    postThumbnailImageView.snp.makeConstraints({ (make) in
      make.top.left.right.bottom.equalToSuperview().priority(999)
      //make.height.equalTo(120)
    })
  }

  func setImageForView(view: UIImageView, image: UIImage?, error: NSError?, cacheType: CacheType, imageUrl: URL?) {
    guard error == nil && image != nil else {
      view.image = nil
      view.backgroundColor = AppColor.LightGray
      return
    }
    view.backgroundColor = AppColor.Clear
    view.image = image
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()

    postThumbnailImageView.image = nil
    postTextLabel.text = nil
    dateLabel.text = nil
    linkPreviewIndicator.isHidden = true
    linkPreviewSitenameLabel.text = nil
    linkPreviewTitleLabel.text = nil
    linkPreviewDescriptionLabel.text = nil
    linkPreviewThumbnail.image = nil
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    
    loadView()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    
    loadView()
  }
}

extension PostCell: DotsViewDelegate {
  func dotsDidTapped(_ dotsView: DotsView) {
    print("more options tapped")
  }
}
