//
//  CategoryHeaderCell.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/18/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import UIKit
import SnapKit
import Kingfisher

class CategoryHeaderView: UIView {
  
  var category: Category?
  
  // UI Element
  var darkBackground = UIView()
  var headerImageView = UIImageView()
  var logoImageView = UIImageView()
  var titleLabel = UILabel()
  var descriptionLabel = UILabel()
  
  func configure(_ object: Category?) {
    self.category = object
    headerImageView.kf.setImage(with: object?.headerImage?.url?.toURL(), options: [.transition(.fade(0.2))])
    logoImageView.kf.setImage(with: object?.image?.url?.toURL(), options: [.transition(.fade(0.2))])
    titleLabel.text = object?.title
    descriptionLabel.text = object?.description
  }
  
  func loadView() {
    self.addSubview(headerImageView)
    self.addSubview(darkBackground)
    self.addSubview(logoImageView)
    self.addSubview(titleLabel)
    self.addSubview(descriptionLabel)

    headerImageView.contentMode = .scaleAspectFill
    darkBackground.layer.opacity = 0.4
    darkBackground.backgroundColor = AppColor.Black

    logoImageView.setCornerRadius(2)
    logoImageView.layer.borderColor = AppColor.White.cgColor
    logoImageView.layer.borderWidth = 3

    titleLabel.textColor = AppColor.White
    titleLabel.font = FontBook.systemFont(.Regular, size: 14)

    descriptionLabel.textColor = AppColor.White
    descriptionLabel.font = FontBook.systemFont(.Regular, size: 12)
    descriptionLabel.numberOfLines = 0
    descriptionLabel.lineBreakMode = .byWordWrapping
    descriptionLabel.textAlignment = .center

    darkBackground.snp.makeConstraints { (make) in
      make.top.left.right.bottom.equalToSuperview()
    }

    headerImageView.snp.makeConstraints { (make) in
      make.top.left.right.bottom.equalToSuperview()
    }

    logoImageView.snp.makeConstraints { (make) in
      make.center.equalToSuperview()
      make.size.equalTo(CGSize(width: 60, height: 60))
    }

    titleLabel.snp.makeConstraints { (make) in
      make.top.equalTo(logoImageView.snp.bottom)
      make.centerX.equalToSuperview()
    }

    descriptionLabel.snp.makeConstraints { (make) in
      make.top.equalTo(titleLabel.snp.bottom)
      make.centerX.equalToSuperview()
      make.left.equalToSuperview().offset(20)
      make.right.equalToSuperview().offset(-20)
    }

  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    
    loadView()
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  init() {
    super.init(frame: CGRect.zero)
    self.translatesAutoresizingMaskIntoConstraints = false
    
    loadView()
  }
}
