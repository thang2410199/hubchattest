//
//  CategoryPostDataSource.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/18/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import UIKit

class CategoryPostDataSource<T, Cell: UITableViewCell>: ArrayDataSouce<T, Cell> where T: ConfigurableCellData, Cell: ConfigurableCell, Cell.Object == T, Cell: ConfigurableCell {
  
  override init(tableView: UITableView, sectionCount: Int) {
    super.init(tableView: tableView, sectionCount: sectionCount)
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: Cell.identifier, for: indexPath) as! Cell
    let object = provider.object(at: indexPath)
    cell.configure(for: object, at: indexPath)
    return cell
  }
  
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    // Trigger load more base on distance to the end of scroll view.
    // Can be changed so it related to the number of posts left or pre load content
    if scrollView.contentSize.height - scrollView.contentOffset.y - scrollView.frame.size.height < 300 {
      Broadcaster.notify(CategoryPostDataSourceDelegate.self, block: { (observer) in
        observer.loadMoreDataTriggered()
      })
    }
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
    Broadcaster.notify(CategoryPostDataSourceDelegate.self, block: { (observer) in
      observer.didSelectCell(indexPath)
    })
  }
  
  func tableView(_ tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: IndexPath) {
    if (!self.provider.cellHeightCache.keys.contains(indexPath.row)) {
      runOnMainThread({ 
        cell.popAnimate()
      })
    }
    self.provider.cellHeightCache[indexPath.row] = cell.bounds.height
  }
  
  func tableView(_ tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
    return self.provider.cellHeightCache[indexPath.row] ?? 300
  }
}

protocol CategoryPostDataSourceDelegate {
  func didSelectCell(_ indexPath: IndexPath)
  func loadMoreDataTriggered()
}
