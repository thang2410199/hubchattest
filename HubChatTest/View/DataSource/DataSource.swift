//
//  DataSource.swift
//
//
//  Created by Thang Ngo Quoc on 11/13/16.
//  Copyright © 2016 . All rights reserved.
//

import UIKit

class DataSource<Provider: DataProvider, Cell: UITableViewCell>: NSObject, UITableViewDataSource, UITableViewDelegate where Cell: ConfigurableCell, Provider.Object == Cell.Object{
  
  let provider: Provider
  let tableView: UITableView
  
  init(tableView: UITableView, provider: Provider) {
    self.tableView = tableView
    self.provider = provider
    super.init()
    
    setup()
  }
  
  func setup() {
    tableView.dataSource = self
    tableView.delegate = self
    tableView.register(Cell.self, forCellReuseIdentifier: Cell.identifier)
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return provider.numberOfItems(section)
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return provider.numberOfSections()
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: Cell.identifier, for: indexPath) as! Cell
    let object = provider.object(at: indexPath)
    cell.configure(for: object, at: indexPath)
    return cell
  }
  
  
  // TODO: Use height caching if needed to improve performance
  //  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
  //    
  //  }
}

class ArrayProvider<T>: DataProvider {
  var objects: [T]
  var cellHeightCache: [Int: CGFloat] = [:]
  let numberOfSection: Int
  var totalObjectToFetch = 0

  init(type: T.Type, numberOfSection: Int) {
    objects = []
    self.numberOfSection = numberOfSection
  }
  
  func numberOfSections() -> Int {
    return self.numberOfSection
  }
  
  func numberOfItems(_ section: Int) -> Int {
    return objects.count
  }
  
  func object(at indexPath: IndexPath) -> T {
    return objects[indexPath.row]
  }
  
  func numberOfRows(_ inSection: Int) -> Int {
    return objects.count
  }
}

class ArrayDataSouce<T, Cell: UITableViewCell>: DataSource<ArrayProvider<T>, Cell> where Cell: ConfigurableCell, Cell.Object == T {
  var objects: [T] {
    return self.provider.objects
  }

  init(tableView: UITableView, sectionCount: Int) {
    let provider = ArrayProvider(type: T.self, numberOfSection: sectionCount)
    super.init(tableView: tableView, provider: provider)
  }

  func add(_ array: [T]) {
    self.provider.objects.append(contentsOf: array)
  }
  
  func addAndUpdate(_ array: [T], section: Int) {
    let oldCount = self.objects.count
    let size = array.count
    self.add(array)
    
    var indexPaths: [IndexPath] = []
    for i in 0..<size {
      indexPaths.append(IndexPath(row: oldCount + i, section: section))
    }
    
    self.tableView.beginUpdates()
    self.tableView.insertRows(at: indexPaths, with: .fade)
    self.tableView.endUpdates()
  }
  
  func add(_ t: T) {
    self.provider.objects.append(t)
  }
  
  func removeAt(_ index: Int) {
    self.provider.objects.remove(at: index)
  }
  
  func clear() {
    self.provider.objects.removeAll()
    self.provider.cellHeightCache.removeAll()
  }

  func updateTotal(_ total: Int) {
    self.provider.totalObjectToFetch = total
  }

  var hasMoreData: Bool {
    return self.provider.objects.count < self.provider.totalObjectToFetch
  }
}
