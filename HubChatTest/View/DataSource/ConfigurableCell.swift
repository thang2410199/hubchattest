//
//  ConfigurableCell.swift
//
//
//  Created by Thang Ngo Quoc on 11/13/16.
//  Copyright © 2016. All rights reserved.
//

import UIKit

protocol ConfigurableCell {
  associatedtype Object
  static var identifier: String { get }
  func configure(for object: Object, at indexPath: IndexPath)
}

protocol ConfigurableCellData {
  var uniqueId: String? {get}
}

protocol DataProvider {
  associatedtype Object
  func numberOfSections() -> Int
  func numberOfRows(_ inSection: Int) -> Int
  func object(at indexPath: IndexPath) -> Object
  func numberOfItems(_ section: Int) -> Int
}
