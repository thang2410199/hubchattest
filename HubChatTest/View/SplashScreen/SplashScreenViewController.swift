//
//  SplashScreenViewController.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/16/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import UIKit
import SnapKit
import TransitionTreasury
import TransitionAnimation

/// this screen will be shown when app launchs. It prepare the resource needed for
/// the app, it might be load user info or prepare for camera
class SplashScreenViewController: TransitionUIViewController {
  
  // MARK: UI element
  let loader: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .white)
  
  override func loadView() {
    super.loadView()
    
    self.view.backgroundColor = AppColor.MainColor
    
    self.view.addSubview(loader)
    
    self.setupAutoLayout()
    
    loader.startAnimating()
    
    // Simulate loading time
    Timer.after(2) {
      // Display the list of post
      let categoryViewController = serviceLocator.getByType(CategoryViewController.self)
      categoryViewController.modalDelegate = self
      categoryViewController.viewModel?.categoryName = "photography"
      let containerViewController = UINavigationController(rootViewController: categoryViewController)
      
      self.tr_presentViewController(containerViewController, method: TRPushTransitionMethod.fade, statusBarStyle: TRStatusBarStyle.lightContent, completion: nil)
    }
  }
  
  func setupAutoLayout() {
    loader.snp.makeConstraints { (make) in
      make.size.equalTo(CGSize(width: 42, height: 42))
      make.center.equalToSuperview()
    }
  }
}
