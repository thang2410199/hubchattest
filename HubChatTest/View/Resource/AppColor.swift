//
//  AppColor.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/17/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import UIKit

struct AppColor {
  static let MainColor = UIColor(hex: "#307EFA")
  static let White = UIColor.white
  static let LightGray = UIColor.lightGray
  static let Clear = UIColor.clear
  static let Black = UIColor.darkGray
}
