//
//  ImagePlaceholder.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/18/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import UIKit

class ImagePlaceholder {
  static let UserAvatar = UIImage.fromColor(AppColor.LightGray, size: CGSize(width: PostCell.AvatarSize, height: PostCell.AvatarSize))
}
