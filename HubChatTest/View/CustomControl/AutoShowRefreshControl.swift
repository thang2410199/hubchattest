//
//  AutoShowRefreshControl.swift
//
//
//  Created by Thang Ngo Quoc on 11/11/16.
//  Copyright © 2016. All rights reserved.
//

import UIKit

class AutoShowRefreshControl: UIRefreshControl {
  weak var hostTableView: UITableView?
  
  func beginRefreshing(_ forcePull: Bool = true) {
    super.beginRefreshing()
    
    if(hostTableView != nil && forcePull) {
      self.hostTableView!.setContentOffset(CGPoint(x: 0, y: self.hostTableView!.contentOffset.y - self.frame.size.height), animated: true)
    }
  }
}
