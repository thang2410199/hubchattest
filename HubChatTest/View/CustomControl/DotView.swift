//
//  DotView.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/19/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import UIKit
import SnapKit

class DotsView: UIStackView {

  fileprivate(set) var dots: [UIView] = []
  fileprivate(set) var numberOfDots: Int = 0
  fileprivate(set) var color: UIColor = UIColor.white
  fileprivate(set) var dotRadius: Double = 4

  var backgroundView: UIView!

  var delegate : DotsViewDelegate?

  var viewBackgroundColor: UIColor = UIColor.clear {
    didSet {
      self.backgroundView.backgroundColor = viewBackgroundColor
    }
  }
    
  required init(coder: NSCoder) {
    super.init(coder: coder)
  }

  init(numberOfDots: Int = 3, color: UIColor = UIColor.white, dotRadius: Double = 4) {
    super.init(frame: CGRect.zero)

    self.translatesAutoresizingMaskIntoConstraints = false
    self.numberOfDots = numberOfDots
    self.color = color
    self.dotRadius = dotRadius
    self.isLayoutMarginsRelativeArrangement = true
    let inset = CGFloat(dotRadius * 2)
    self.layoutMargins = UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
    self.isUserInteractionEnabled = true
    self.distribution = .equalCentering
    self.alignment = .center
    self.axis = .horizontal

    loadView()
  }

  func beginAnimate() {
    if(self.backgroundView.backgroundColor == self.color) {
      return
    }
    UIView.animate(withDuration: 0.1, animations: {
      self.backgroundView.backgroundColor = self.color
      for view in self.dots {
        view.backgroundColor = UIColor.white
      }
    }) 
  }

  func endAnimate() {
    if(self.backgroundView.backgroundColor == UIColor.clear) {
      return
    }
    UIView.animate(withDuration: 0.2, animations: {
      self.backgroundView.backgroundColor = UIColor.clear
      for view in self.dots {
        view.backgroundColor = self.color
      }
    }) 
  }

  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    beginAnimate()
  }

  override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    if let touch = touches.first {
      let point = touch.location(in: self)
      if (!self.bounds.contains(point)) {
        endAnimate()
      }
      else {
        beginAnimate()
      }
    }
  }

  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    endAnimate()

    delegate?.dotsDidTapped(self)
  }

  override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
    print("touchesCancelled")
    endAnimate()
  }

  func loadView() {

    backgroundView = UIView()
    backgroundView.layer.cornerRadius = CGFloat(self.dotRadius)
    self.addSubview(backgroundView)

    for _ in 0 ..< numberOfDots {
      let view = UIView()
      view.backgroundColor = self.color
      view.layer.cornerRadius = CGFloat(self.dotRadius)
      self.addArrangedSubview(view)
      self.addSubview(view)
      dots.append(view)
      view.snp.makeConstraints({ (make) in
        make.size.equalTo(CGSize(width: dotRadius * 2, height: dotRadius * 2))
      })
    }

    backgroundView.snp.makeConstraints({ (make) in
      make.top.left.right.bottom.equalToSuperview()
    })
  }
}

protocol DotsViewDelegate {
  func dotsDidTapped(_ dotsView: DotsView)
}

