//
//  ServiceLocator.swift
//
//
//  Created by Ngo Quoc Thang on 12/17/16.
//  Copyright © 2016 . All rights reserved.
//

import UIKit
import Swinject

/// Adapter for Swinject
/// Register all the needed service and interface
class ServiceLocator: AnyObject {
  let container = Container()
  
  init() {
    
    self.container.register(IUnauthorizedService.self, name: nil) { (r: Resolver) in
      UnauthorizedService()
      }.inObjectScope(.container)
    
    
    self.container.register(IStorageCacheService.self, name: nil) { (r: Resolver) in
      JsonStorageCacheService()
      }.inObjectScope(.container)
    
    self.container.register(ICategoryViewModel.self, name: nil) { (r: Resolver) in
      // Fake network service to mock data in offline mode
      CategoryViewModel(
        unauthorizedService: r.resolve(IUnauthorizedService.self)!,
        storageService: r.resolve(IStorageCacheService.self)!)
      }.inObjectScope(.transient)
    
    self.container.register(SplashScreenViewController.self, name: nil) { (r: Resolver) in
      SplashScreenViewController()
      }.inObjectScope(.transient)
    
    self.container.register(CategoryViewController.self) { (r: Resolver)  in
      CategoryViewController(viewModel: r.resolve(ICategoryViewModel.self)!)
      }.inObjectScope(.transient)
  }
  
  /**
   Get an instane of object
   
   - parameter identifier: identifier of object, if nil is given always
   
   - returns: instance of object
   */
  func getInstance<T>(_ identifier: String?) -> T? {
    return self.container.resolve(T.self, name: identifier)
  }
  
  func getByType<T>(_ type: T.Type, identifier: String? = nil) -> T {
    return self.container.resolve(type, name: identifier)!
  }
}
