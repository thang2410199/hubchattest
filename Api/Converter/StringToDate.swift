//
//  StringToDate.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/19/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import Foundation
import ObjectMapper

open class StringDateTransform: TransformType {
  public typealias Object = Date
  public typealias JSON = String

  public init() {}

  open func transformFromJSON(_ value: Any?) -> Date? {
    if let time = value as? String {
      return Date.fromString(time, format: ApiEndPoints.DateTimeFormat)
    }

    return nil
  }

  open func transformToJSON(_ value: Date?) -> String? {
    if let date = value {
      return date.toString(ApiEndPoints.DateTimeFormat)
    }
    return nil
  }
}
