//
//  BaseWebService.swift
//
//
//  Created by Ngo Quoc Thang on 8/5/16.
//  Copyright © 2016. All rights reserved.
//

import ObjectMapper
import Alamofire
import AlamofireObjectMapper
import Foundation

class BaseWebService: NSObject, IWebRequestService {
  
  var manager: SessionManager = Alamofire.SessionManager.default
  
  let queue = DispatchQueue(label: "network-manager-response-queue", attributes: DispatchQueue.Attributes.concurrent)
  
  internal func get<T: Mappable>(_ parameters: [String: Any]? = nil, path: String, queue: DispatchQueue? = nil, error: Error?, completeWith: @escaping (_ result: T) -> Void) {
    if let parameters = parameters {
      print("GET " + path + " with parameter")
      print(parameters)
    }
    else {
      print("GET " + path + " with no parameter")
    }
    self.manager.request(path, method: .get, parameters: parameters, encoding: URLEncoding(destination: .methodDependent)).responseObject(queue: queue) {
      (mapperResonse: DataResponse<T>) in
      guard mapperResonse.response?.statusCode.isSuccess() == true else {
        if let data = mapperResonse.data {
          let json = String(data: data, encoding: String.Encoding.utf8)
          print("Failure Response: \(json)")
        }
        
        if let statusCode = mapperResonse.response?.statusCode {
          switch (statusCode) {
          case 404:
            error?(.notFound)
          case 400:
            error?(.badRequest)
          case 401:
            error?(.unauthorizedRequest)
          default:
            error?(.serverError)
          }
          
          print("network error with status code \(statusCode)")
        }
        else {
          error?(ErrorReason.serverError)
        }
        
        //print(mapperResonse.result.debugDescription)
        return
      }
      
      guard mapperResonse.result.error == nil else {
        error?(ErrorReason.clientError)
        return
      }
      
      if let result = mapperResonse.result.value {
        completeWith(result)
      }
      else {
        error?(.clientError)
      }
    }
  }
  
  func post(_ path: String, parameter: [String: Any]?, error: Error?, success: @escaping () -> Void) {
    self.request(.post, path: path, parameters: parameter, queue: self.queue, error: error, completeWith: success)
  }
  
  func put(_ path: String, parameter: [String: Any]?, error: Error?, success: @escaping () -> Void) {
    self.request(.put, path: path, parameters: parameter, queue: self.queue, error: error, completeWith: success)
  }
  
  func delete(_ path: String, error: Error?, success: @escaping () -> Void) {
    self.request(.delete, path: path, parameters: nil, queue: self.queue, error: error, completeWith: success)
  }
  
  func request<T: Mappable>(_ method: Alamofire.HTTPMethod, path: String, parameters: [String: Any]? = nil, queue: DispatchQueue? = nil, error: Error?, completeWith: @escaping (_ result: T) -> Void) {
    print(method.rawValue + " " + path)
    self.manager.request(path, method: .get, parameters: parameters, encoding: URLEncoding(destination: .methodDependent)).responseObject(queue: queue) {
      (mapperResonse: DataResponse<T>) in
      guard mapperResonse.response?.statusCode.isSuccess() == true else {
        if let statusCode = mapperResonse.response?.statusCode {
          switch (statusCode) {
          case 404:
            error?(.notFound)
          case 400:
            error?(.unauthorizedRequest)
          case 401:
            error?(.badRequest)
          default:
            error?(.serverError)
          }
          
          print("\(statusCode)")
        }
        else {
          error?(.serverError)
        }
        
        //print(mapperResonse.result.debugDescription)
        return
      }
      
      guard mapperResonse.result.error == nil else {
        error?(.clientError)
        return
      }
      
      if let result = mapperResonse.result.value {
        completeWith(result)
      }
      else {
        error?(.clientError)
      }
    }
  }
  
  func request(_ method: Alamofire.HTTPMethod, path: String, parameters: [String: Any]? = nil, queue: DispatchQueue? = nil, error: Error?, completeWith: @escaping () -> Void) {
    print(method.rawValue + " " + path)
    self.manager.request(path, method: .get, parameters: parameters, encoding: URLEncoding(destination: .methodDependent)).response {
      response in
      guard response.response?.statusCode.isSuccess() == true else {
        if let statusCode = response.response?.statusCode {
          switch (statusCode) {
          case 404:
            error?(.notFound)
          case 400:
            error?(.unauthorizedRequest)
          case 401:
            error?(.badRequest)
          default:
            error?(.serverError)
          }
          
          print("\(statusCode)")
        }
        else {
          error?(.serverError)
        }
        return
      }
      
      guard response.error == nil else {
        error?(.clientError)
        print("error with \(response.error!)")
        return
      }
      
      completeWith()
    }
  }
}
