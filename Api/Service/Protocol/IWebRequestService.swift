//
//  IWebRequestService.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/18/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import ObjectMapper

protocol IWebRequestService {
  func get<T: Mappable>(_ parameters: [String: Any]?, path: String, queue: DispatchQueue?, error: Error?, completeWith: @escaping (_ result: T) -> Void)
  
  func post(_ path: String, parameter: [String: Any]?, error: Error?, success: @escaping () -> Void)
  
  func put(_ path: String, parameter: [String: Any]?, error: Error?, success: @escaping () -> Void)
  
  func delete(_ path: String, error: Error?, success: @escaping () -> Void)
}
