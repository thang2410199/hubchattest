//
//  IUnauthorizedService.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/17/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

/**
 *  Protocol for public api service
 */
public protocol IUnauthorizedService {
  func getPosts(_ category: String, parameter: [String: AnyObject]?, error: Error?, completion: @escaping (_ result: GetPostsResponse) -> Void)
  
  func getCategory(_ category: String, error: Error?, completion: @escaping (_ result: GetCategoryResponse) -> Void)
}
