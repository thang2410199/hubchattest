//
//  NetworkError.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/18/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import Foundation

public typealias Error = (_ reason: ErrorReason) -> Void
public typealias DefaultError = (_ error: NSError) -> Void
public typealias DefaultNetworkComplete = (_ data: Data?, _ urlResponse: URLResponse?, _ error: NSError?) -> Void

public enum ErrorReason: Int {
  case serverError = 500
  case clientError = 1991
  case unauthorizedRequest = 401
  case badRequest = 400
  case notFound = 404
  
  var description: String {
    switch self {
    case .serverError:
      return "server error"
    case .clientError:
      return "client error"
    case .unauthorizedRequest:
      return "unauthorized request"
    case .badRequest:
      return "bad request"
    default:
      return "unkown error"
    }
  }
}
