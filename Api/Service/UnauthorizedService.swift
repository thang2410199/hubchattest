//
//  UnauthorizedService.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/17/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class UnauthorizedService: BaseWebService, IUnauthorizedService {
  func getPosts(_ category: String, parameter: [String: AnyObject]? = nil, error: Error?, completion: @escaping (_ result: GetPostsResponse) -> Void) {
    self.get(parameter, path: ApiEndPoints.getPost(category: category).path, error: error, completeWith: completion)
  }
  
  func getCategory(_ category: String, error: Error?, completion: @escaping (_ result: GetCategoryResponse) -> Void) {
    self.get(path: ApiEndPoints.getCategoryInfo(category: category).path, error: error, completeWith: completion)
  }
}
