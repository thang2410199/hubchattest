//
//  CategorySettings.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/16/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import Foundation
import ObjectMapper

public struct CategorySettings: Mappable {
  
  static let kSettingsWhoCanJoinKey: String = "whoCanJoin"
  static let kSettingsWhoCanCreateTopicsKey: String = "whoCanCreateTopics"
  static let kSettingsFlagAsNsfwKey: String = "flagAsNsfw"
  static let kSettingsIgnoreQueryStringKey: String = "ignoreQueryString"
  static let kSettingsWhoCanSeeKey: String = "whoCanSee"
  static let kSettingsAllowGuestPostingKey: String = "allowGuestPosting"
  
  public var whoCanJoin: String?
  public var whoCanCreateTopics: String?
  public var flagAsNsfw: Bool?
  public var ignoreQueryString: Bool?
  public var whoCanSee: String?
  public var allowGuestPosting: Bool?
  
  public init?(map: Map) {
    
  }
  
  public mutating func mapping(map: Map) {
    whoCanJoin <- map[CategorySettings.kSettingsWhoCanJoinKey]
    whoCanCreateTopics <- map[CategorySettings.kSettingsWhoCanCreateTopicsKey]
    flagAsNsfw <- map[CategorySettings.kSettingsFlagAsNsfwKey]
    ignoreQueryString <- map[CategorySettings.kSettingsIgnoreQueryStringKey]
    whoCanSee <- map[CategorySettings.kSettingsWhoCanSeeKey]
    allowGuestPosting <- map[CategorySettings.kSettingsAllowGuestPostingKey]
  }
}
