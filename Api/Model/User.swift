//
//  User.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/16/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import Foundation
import ObjectMapper

public struct User: Mappable {
  
  static let kUserUuidKey: String = "uuid"
  static let kUserInternalIdentifierKey: String = "id"
  static let kUserDisplayNameKey: String = "displayName"
  static let kUserFamilyNameKey: String = "familyName"
  static let kUserDescriptionValueKey: String = "description"
  static let kUserGivenNameKey: String = "givenName"
  static let kUserAvatarKey: String = "avatar"
  static let kUserUsernameKey: String = "username"
  
  public var uuid: String?
  public var internalIdentifier: String?
  public var displayName: String?
  public var familyName: String?
  public var descriptionValue: String?
  public var givenName: String?
  public var avatar: RemoteImage?
  public var username: String?
  
  public init?(map: Map) {
    
  }
  
  public mutating func mapping(map: Map) {
    uuid <- map[User.kUserUuidKey]
    internalIdentifier <- map[User.kUserInternalIdentifierKey]
    displayName <- map[User.kUserDisplayNameKey]
    familyName <- map[User.kUserFamilyNameKey]
    descriptionValue <- map[User.kUserDescriptionValueKey]
    givenName <- map[User.kUserGivenNameKey]
    avatar <- map[User.kUserAvatarKey]
    username <- map[User.kUserUsernameKey]
  }
}
