//
//  PostPreview.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/17/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import Foundation
import ObjectMapper

public struct PostPreview: Mappable {
  
  static let kOrigLink = "origLink"
  static let kLink = "link"
  static let kHost = "host"
  static let kType = "type"
  static let kContent = "content"
  
  public var originalLink: String?
  public var link: String?
  public var host: String?
  public var type: String?
  public var content: PostPreviewContent?
  
  public init?(map: Map){
    
  }
  
  public mutating func mapping(map: Map) {
    originalLink <- map[PostPreview.kOrigLink]
    link <- map[PostPreview.kLink]
    host <- map[PostPreview.kHost]
    type <- map[PostPreview.kType]
    content <- map[PostPreview.kContent]
  }
}
