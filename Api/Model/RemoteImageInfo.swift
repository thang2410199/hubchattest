//
//  RemoteImageInfo.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/19/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import Foundation
import ObjectMapper

public struct RemoteImageInfo: Mappable {

  static let kWidth = "width"
  static let kHeight = "height"
  static let kType = "contentType"

  public var type: String?
  public var width: Int?
  public var height: Int?

  public init?(map: Map){

  }

  public mutating func mapping(map: Map) {
    type <- map[RemoteImageInfo.kType]
    width <- map[RemoteImageInfo.kWidth]
    height <- map[RemoteImageInfo.kHeight]
  }
}
