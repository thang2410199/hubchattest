//
//  PostPreviewContent.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/17/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import Foundation
import ObjectMapper

public struct PostPreviewContent: Mappable {
  
  static let kTitle = "title"
  static let kDescription = "description"
  static let kImage = "image"
  static let kImageInfo = "imageInfo"
  static let kSite = "site"
  
  public var title: String?
  public var description: String?
  public var imageInfo: RemoteImageInfo?
  public var image: String?
  public var site: PostPreviewContentSite?
  
  public init?(map: Map){
    
  }
  
  public mutating func mapping(map: Map) {
    title <- map[PostPreviewContent.kTitle]
    description <- map[PostPreviewContent.kDescription]
    image <- map[PostPreviewContent.kImage]
    site <- map[PostPreviewContent.kSite]
    imageInfo <- map[PostPreviewContent.kImageInfo]
  }
}
