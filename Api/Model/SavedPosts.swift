//
//  SavedPosts.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/19/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import Foundation
import ObjectMapper

struct SavedPosts: Mappable {
  static let kPosts = "posts"
  
  var posts: [Post]
  
  mutating func mapping(map: Map) {
    posts <- map[SavedPosts.kPosts]
  }
  
  init(posts: [Post]) {
    self.posts = posts
  }
  
  init?(map: Map) {
    posts = []
    posts <- map[SavedPosts.kPosts]
  }
}
