//
//  PostSortStat.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/17/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import Foundation
import ObjectMapper

public struct PostSortStat: Mappable {
  
  internal let kSortControversyScoreKey = "controversyScore"
  internal let kSortWilsonScoreKey = "wilsonScore"
  internal let kSortHotScoreKey = "hotScore"
  internal let kSortTotalCommentsHotScoreKey = "totalCommentsHotScore"
  
  public var controversyScore: Int?
  public var wilsonScore: Float?
  public var hotScore: Float?
  public var totalCommentsHotScore: Float?
  
  
  
  // MARK: ObjectMapper Initalizers
  /**
   Map a JSON object to this class using ObjectMapper
   - parameter map: A mapping from ObjectMapper
   */
  public init?(map: Map){
    
  }
  
  /**
   Map a JSON object to this class using ObjectMapper
   - parameter map: A mapping from ObjectMapper
   */
  public mutating func mapping(map: Map) {
    controversyScore <- map[kSortControversyScoreKey]
    wilsonScore <- map[kSortWilsonScoreKey]
    hotScore <- map[kSortHotScoreKey]
    totalCommentsHotScore <- map[kSortTotalCommentsHotScoreKey]
    
  }
}
