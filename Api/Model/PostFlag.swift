//
//  PostFlag.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/17/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import Foundation
import ObjectMapper

public struct PostFlag: Mappable {
  
  static let kFlagsIsWaitingForApprovalKey: String = "isWaitingForApproval"
  static let kFlagsIsLockedKey: String = "isLocked"
  static let kFlagsIsFeaturedKey: String = "isFeatured"
  static let kFlagsIsStickyKey: String = "isSticky"
  static let kFlagsIsFlaggedKey: String = "isFlagged"
  
  public var isWaitingForApproval: Bool?
  public var isLocked: Bool?
  public var isFeatured: Bool?
  public var isSticky: Bool?
  public var isFlagged: Bool?
  
  public init(locked: Bool) {
    isWaitingForApproval = true
    isLocked = locked
    isFeatured = true
    isSticky = false
    isFlagged = false
  }
  
  public init?(map: Map) {
    
  }
  
  public mutating func mapping(map: Map) {
    isWaitingForApproval <- map[PostFlag.kFlagsIsWaitingForApprovalKey]
    isLocked <- map[PostFlag.kFlagsIsLockedKey]
    isFeatured <- map[PostFlag.kFlagsIsFeaturedKey]
    isSticky <- map[PostFlag.kFlagsIsStickyKey]
    isFlagged <- map[PostFlag.kFlagsIsFlaggedKey]
  }
}
