//
//  PostEntityImage.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/17/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import Foundation
import ObjectMapper

public struct PostEntityImage: Mappable {
  
  static let kSize = "size"
  static let kType = "type"
  static let kCdnUrl = "cdnUrl"
  static let kWidth = "width"
  static let kHeight = "height"
  static let kLocation = "location"
  
  // Size of image in byte?
  var size: Int64?
  var type: String?
  var cdnUrl: String?
  var width: Int?
  var height: Int?
  //var location:
  
  public init?(map: Map) {
    
  }
  
  public mutating func mapping(map: Map) {
    size <- map[PostEntityImage.kSize]
    type <- map[PostEntityImage.kType]
    cdnUrl <- map[PostEntityImage.kCdnUrl]
    width <- map[PostEntityImage.kWidth]
    height <- map[PostEntityImage.kHeight]
  }
}
