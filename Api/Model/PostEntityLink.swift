//
//  PostEntityLink.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/17/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import Foundation
import ObjectMapper

public struct PostEntityLink: Mappable {
  
  static let kLinksLinkKey = "link"
  static let kLinksIndicesKey = "indices"
  
  public var link: String?
  public var indices: [Int]?
  
  public init?(map: Map){
    
  }
  
  public mutating func mapping(map: Map) {
    link <- map[PostEntityLink.kLinksLinkKey]
    indices <- map[PostEntityLink.kLinksIndicesKey]
  }
}
