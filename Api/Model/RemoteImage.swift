//
//  RemoteImage.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/16/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import Foundation
import ObjectMapper

public struct RemoteImage: Mappable {
  
  static let kRemoteImageCropKey: String = "crop"
  static let kRemoteImageUrlKey: String = "url"
  
  public var crop: RemoteImageCrop?
  public var url: String?
  
  public init?(map: Map) {
    crop <- map[RemoteImage.kRemoteImageCropKey]
    url <- map[RemoteImage.kRemoteImageUrlKey]
  }
  
  public mutating func mapping(map: Map) {
    
  }
}
