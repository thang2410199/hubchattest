//
//  Post.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/16/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import Foundation
import ObjectMapper

public struct Post: Mappable, ConfigurableCellData {
  
  var uniqueId: String? {
    return self.uuid
  }
  
  static let kPostsTitleKey = "title"
  static let kPostsUpdatedAtKey = "updatedAt"
  static let kPostsChannelKey = "channel"
  static let kPostsRawContentKey = "rawContent"
  static let kPostsGuestUserAliasKey = "guestUserAlias"
  static let kPostsLocationKey = "location"
  static let kPostsForumKey = "forum"
  static let kPostsTimestampKey = "timestamp"
  static let kPostsCreatedByUUIDKey = "createdByUUID"
  static let kPostsCreatedAtKey = "createdAt"
  static let kPostsStatsKey = "stats"
  static let kPostsSortKey = "sort"
  static let kPostsIdentifierKey = "id"
  static let kPostsLinksKey = "links"
  static let kPostsEntitiesKey = "entities"
  static let kPostsChannelUUIDKey = "channelUUID"
  static let kPostsForumUUIDKey = "forumUUID"
  static let kPostsFlagsKey = "flags"
  static let kPostsCreatedByKey = "createdBy"
  static let kPostsCacheKey = "cache"
  static let kPostsImagesKey = "images"
  static let kPostsLinkPreviewKey = "linkPreview"
  
  
  // MARK: Properties
  public var title: String?
  public var updatedAt: String?
  //public var channel: Channel?
  public var rawContent: String?
  //public var guestUserAlias: GuestUserAlias?
  //public var location: Location?
  public var category: Category?
  public var timestamp: Int64?
  public var createdAt: String?
  public var createdDate: Date?
  public var stats: PostStat?
  public var sort: PostSortStat?
  public var uuid: String?
  //public var links: [Links]?
  public var entities: PostEntity?
  //public var channelUUID: ChannelUUID?
  //public var forumUUID: String?
  public var flags: PostFlag?
  public var creator: User?
  public var cache: PostCache?
  //public var images: []?
  public var preview: PostPreview?
  
  public init?(map: Map) {
    
  }
  
  public init(uuid: String) {
    self.uuid = uuid
  }
  
  
  public mutating func mapping(map: Map) {
    title <- map[Post.kPostsTitleKey]
    updatedAt <- map[Post.kPostsUpdatedAtKey]
    //channel <- map[Post.kPostsChannelKey]
    rawContent <- map[Post.kPostsRawContentKey]
    //guestUserAlias <- map[Post.kPostsGuestUserAliasKey]
    //location <- map[Post.kPostsLocationKey]
    category <- map[Post.kPostsForumKey]
    timestamp <- (map[Post.kPostsTimestampKey], TransformOf<Int64, NSNumber>(fromJSON: { $0?.int64Value }, toJSON: { $0.map { NSNumber(value: $0) } }))
    //createdByUUID <- map[Post.kPostsCreatedByUUIDKey]
    createdAt <- map[Post.kPostsCreatedAtKey]
    createdDate <- (map[Post.kPostsCreatedAtKey], StringDateTransform())
    stats <- map[Post.kPostsStatsKey]
    sort <- map[Post.kPostsSortKey]
    uuid <- map[Post.kPostsIdentifierKey]
    //links <- map[Post.kPostsLinksKey]
    entities <- map[Post.kPostsEntitiesKey]
    //channelUUID <- map[Post.kPostsChannelUUIDKey]
    //forumUUID <- map[Post.kPostsForumUUIDKey]
    flags <- map[Post.kPostsFlagsKey]
    //createdBy <- map[Post.kPostsCreatedByKey]
    cache <- map[Post.kPostsCacheKey]
    //images <- map[Post.kPostsImagesKey]
    preview <- map[Post.kPostsLinkPreviewKey]
    creator <- map[Post.kPostsCreatedByKey]
  }
}
