//
//  RemoteImageCrop.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/16/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import Foundation
import ObjectMapper

public struct RemoteImageCrop: Mappable {
  
  static let kCropHeightKey: String = "height"
  static let kCropXKey: String = "x"
  static let kCropWidthKey: String = "width"
  static let kCropYKey: String = "y"
  
  public var height: Int?
  public var x: Int?
  public var width: Int?
  public var y: Int?
  
  public init?(map: Map) {
    
  }
  
  public mutating func mapping(map: Map) {
    height <- map[RemoteImageCrop.kCropHeightKey]
    x <- map[RemoteImageCrop.kCropXKey]
    width <- map[RemoteImageCrop.kCropWidthKey]
    y <- map[RemoteImageCrop.kCropYKey]
  }
}
