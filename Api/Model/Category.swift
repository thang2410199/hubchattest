//
//  Category.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/16/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import Foundation
import ObjectMapper

public struct Category: Mappable {
  
  static let kUuid = "uuid"
  static let kUpdatedAt = "updatedAt"
  static let kCreatedAt = "createdAt"
  static let kSlug = "slug"
  static let kTitle = "title"
  static let kBannedCount = "bannedCount"
  static let kDescription = "description"
  static let kCreatedBy = "createdBy"
  static let kOwner = "owner"
  static let kLocation = "location"
  static let kAdIntegrations = "adIntegrations"
  static let kEmbedded = "embedded"
  static let kSetting = "settings"
  static let kBlacklistedWords = "blacklistedWords"
  static let kInternalIdentifier = "id"
  static let kBranding = "branding"
  static let kChannels = "channels"
  static let kJoinRequestCount = "joinRequestCount"
  static let kTimestamp = "timestamp"
  static let kImage = "image"
  static let kModerators = "moderators"
  static let kPostCount = "postCount"
  static let kFlaggedCount = "flaggedCount"
  static let kMemberCount = "memberCount"
  static let kHeaderImage = "headerImage"
  
  
  // MARK: Properties
  public var title: String?
  public var owner: User?
  public var uuid: String?
  public var bannedCount: Int?
  public var updatedAt: String?
  public var description: String?
  //public var location: Location?
  public var settings: CategorySettings?
  //public var blacklistedWords: []?
  //public var branding: Branding?
  //public var channels: []?
  public var embedded: CategoryEmbedded?
  public var slug: String?
  //public var adIntegrations: []?
  public var joinRequestCount: Int?
  //public var timestamp: Int64?
  public var image: RemoteImage?
  public var createdAt: String?
  // should use collection of User
  public var moderators: [String]?
  public var postCount: Int?
  public var flaggedCount: Int?
  public var memberCount: Int?
  public var createdBy: User?
  public var headerImage: RemoteImage?
  
  public init?(map: Map) {
    
  }
  
  
  public mutating func mapping(map: Map) {
    uuid <- map[Category.kUuid]
    owner <- map[Category.kOwner]
    title <- map[Category.kTitle]
    owner <- map[Category.kOwner]
    createdBy <- map[Category.kCreatedBy]
    moderators <- map[Category.kModerators]
    joinRequestCount <- map[Category.kJoinRequestCount]
    slug <- map[Category.kSlug]
    postCount <- map[Category.kPostCount]
    flaggedCount <- map[Category.kFlaggedCount]
    memberCount <- map[Category.kMemberCount]
    description <- map[Category.kDescription]
    settings <- map[Category.kSetting]
    embedded <- map[Category.kEmbedded]
    image <- map[Category.kImage]
    headerImage <- map[Category.kHeaderImage]
  }
}
