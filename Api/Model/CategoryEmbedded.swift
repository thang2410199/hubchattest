//
//  CategoryEmbedded.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/16/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import Foundation
import ObjectMapper

public struct CategoryEmbedded: Mappable {
  
  static let kEmbeddedAllowedUrlsKey: String = "allowedUrls"
  static let kEmbeddedColorKey: String = "color"
  
  //public var allowedUrls: []?
  public var color: String?
  
  public init?(map: Map) {
    
  }
  
  public mutating func mapping(map: Map) {
    //allowedUrls <- map[CategoryEmbedded.kEmbeddedAllowedUrlsKey]
    color <- map[CategoryEmbedded.kEmbeddedColorKey]
  }
}
