//
//  PostPreviewContentSite.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/17/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import Foundation
import ObjectMapper

public struct PostPreviewContentSite: Mappable {
  
  static let kName = "name"
  
  public var name: String?
  
  public init?(map: Map){
    
  }
  
  public mutating func mapping(map: Map) {
    name <- map[PostPreviewContentSite.kName]
  }
}
