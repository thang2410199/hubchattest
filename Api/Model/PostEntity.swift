//
//  PostEntity.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/17/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import Foundation
import ObjectMapper

public struct PostEntity: Mappable {
  
  static let kEntitiesVideosKey = "videos"
  static let kEntitiesFilesKey = "files"
  static let kEntitiesImagesKey = "images"
  static let kEntitiesLinksKey = "links"
  static let kEntitiesMentionsKey = "mentions"
  
  //public var videos: [Video]?
  //public var files: [File]?
  public var images: [PostEntityImage]?
  public var links: [PostEntityLink]?
  //public var mentions: [Mention]?
  
  public init?(map: Map){
    
  }
  
  public mutating func mapping(map: Map) {
    //videos <- map[kEntitiesVideosKey]
    //files <- map[kEntitiesFilesKey]
    images <- map[PostEntity.kEntitiesImagesKey]
    links <- map[PostEntity.kEntitiesLinksKey]
    //mentions <- map[kEntitiesMentionsKey]
    
  }
  
}
