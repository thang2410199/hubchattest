//
//  PostCache.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/17/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import Foundation
import ObjectMapper

public struct PostCache: Mappable {
  
  static let kTotalTop = "totalTopLevelComments"
  static let kTotalComment = "totalComments"
  
  public var totalTopLevelComment: Int?
  public var totalComment: Int?
  
  public init?(map: Map){
    
  }
  
  public mutating func mapping(map: Map) {
    totalTopLevelComment <- map[PostCache.kTotalTop]
    totalComment <- map[PostCache.kTotalComment]
  }
}
