//
//  PostStat.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/17/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import Foundation
import ObjectMapper

public struct PostStat: Mappable {
  
  static let kStatsNegativityKey = "negativity"
  static let kStatsUpVotesKey = "upVotes"
  static let kStatsDownVotesKey = "downVotes"
  
  public var negativity: Int?
  public var upVotes: Int?
  public var downVotes: Int?
  
  public init?(map: Map) {
    
  }
  
  public mutating func mapping(map: Map) {
    negativity <- map[PostStat.kStatsNegativityKey]
    upVotes <- map[PostStat.kStatsUpVotesKey]
    downVotes <- map[PostStat.kStatsDownVotesKey]
    
  }
}
