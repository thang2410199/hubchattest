//
//  ApiEndPoints.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/16/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import Foundation
import Alamofire

enum ApiEndPoints {

  static let hostname = "https://api.hubchat.com"
  static let DateTimeFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

  case getPost(category: String)
  case getCategoryInfo(category: String)
  
  var path: String {
    switch self {
    case .getCategoryInfo(let category):
      return ApiEndPoints.hostname + "/v1/forum/" + category
    case .getPost(let category):
      return ApiEndPoints.hostname + "/v1/forum/" + category + "/post"
    }
  }
  
  var method: Alamofire.HTTPMethod {
    switch self {
    case .getPost,
       .getCategoryInfo:
      return Alamofire.HTTPMethod.get
    }
  }
}

enum RequestStatus: String {
  case Ok = "ok"
  // replace with correct value since api doesn't list all possible value
  case Error = "error"
  case Unknown = ""
}
