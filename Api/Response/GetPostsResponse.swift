//
//  GetPostsResponse.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/16/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import Foundation
import ObjectMapper

public struct GetPostsResponse: Mappable {
  static let kStatus = "status"
  static let kPosts = "posts"
  static let kMeta = "meta"
  
  var status: RequestStatus?
  var posts: [Post]?
  var meta: GetPostsMeta?
  
  public init?(map: Map) {
    
  }
  
  public mutating func mapping(map: Map) {
    self.status <- map[GetPostsResponse.kStatus]
    self.posts <- map[GetPostsResponse.kPosts]
    self.meta <- map[GetPostsResponse.kMeta]
  }
}
