//
//  GetCategoryResponse.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/16/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import Foundation
import ObjectMapper

public struct GetCategoryResponse: Mappable {
  static let kStatus = "status"
  static let kForum = "forum"
  
  var status: RequestStatus?
  var category: Category?
  
  public init?(map: Map) {
    
  }
  
  public mutating func mapping(map: Map) {
    self.status <- map[GetCategoryResponse.kStatus]
    self.category <- map[GetCategoryResponse.kForum]
  }
}
