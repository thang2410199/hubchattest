//
//  GetPostsMeta.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/16/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import Foundation
import ObjectMapper

public struct GetPostsMeta: Mappable {
  static let kCount = "count"
  static let kTotalCount = "totalCount"
  
  var count: Int?
  var total: Int?
  
  public init?(map: Map) {
    
  }
  
  public mutating func mapping(map: Map) {
    count <- map[GetPostsMeta.kCount]
    total <- map[GetPostsMeta.kTotalCount]
  }
}
