//
//  JsonStorageCacheService.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/17/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import Foundation
import ObjectMapper

/// Cache objects into storage using json format, suitable for trivial stuffs
/// For more complex need use other engine like Ream DB
class JsonStorageCacheService: IStorageCacheService {
  
  func saveJson<T: Mappable>(_ object: T, key: String) {
    // Serialize to string
    let JSONString = Mapper().toJSONString(object, prettyPrint: true)
    
    // Write string to file sutostream.dat
    let filePath = self.getDocumentsDirectory().appendingPathComponent(key)
    
    do {
      try JSONString!.write(toFile: filePath, atomically: true, encoding: String.Encoding.utf8)
    } catch {
      // failed to write file – bad permissions, bad filename, missing permissions, or more likely it can't be converted to the encoding
    }
  }
  
  func loadJson<T: Mappable>(_ key: String) -> T? {
    // Load string from file
    let filePath = self.getDocumentsDirectory().appendingPathComponent(key)
    
    //reading
    do {
      // Deserialize to object
      let jsonString = try String(contentsOfFile: filePath, encoding: String.Encoding.utf8)
      return Mapper<T>().map(JSONString: jsonString)!
    }
    catch {
      return nil
    }
  }
  
  func getDocumentsDirectory() -> NSString {
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
    let documentsDirectory = paths[0]
    return documentsDirectory as NSString
  }
}
