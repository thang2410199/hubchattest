//
//  IStorageCacheService.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/17/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import Foundation
import ObjectMapper
/**
 *  Protocol to define functions for cache objects to storage
 */
protocol IStorageCacheService {
  /**
   Store object as Json to disk
   
   - parameter object: object comfort Mappable protocol
   - parameter key:  name of the stored file
   
   - returns: Void
   */
  func saveJson<T: Mappable>(_ object: T, key: String)
  
  /**
   Load json file from disk can convert back to object
   
   - parameter key: file name
   
   - returns: if file not found, return nil
   */
  func loadJson<T: Mappable>(_ key: String) -> T?
}

/// Object that can be saved using encoding
open class NSSaveableObject: NSObject, NSCoding {
  override init() {}
  
  required public init(coder aDecoder: NSCoder) {
  }
  
  open func encode(with aCoder: NSCoder) {
  }
}
