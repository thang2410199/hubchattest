//
//  Number.swift
//  HubChatTest
//
//  Created by Ngo Quoc Thang on 12/18/16.
//  Copyright © 2016 Ngo Quoc Thang. All rights reserved.
//

import UIKit

public extension Float {
  /// Returns a random floating point number between 0.0 and 1.0, inclusive.
  public static var random:Float {
    get {
      return Float(arc4random()) / 0xFFFFFFFF
    }
  }
  /**
   Create a random num Float
   
   - parameter min: Float
   - parameter max: Float
   
   - returns: Float
   */
  public static func random(min: Float, max: Float) -> Float {
    return Float.random * (max - min) + min
  }
}

public extension CGFloat {
  /// Randomly returns either 1.0 or -1.0.
  public static var randomSign:CGFloat {
    get {
      return (arc4random_uniform(2) == 0) ? 1.0 : -1.0
    }
  }
  /// Returns a random floating point number between 0.0 and 1.0, inclusive.
  public static var random:CGFloat {
    get {
      return CGFloat(Float.random)
    }
  }
  /**
   Create a random num CGFloat
   
   - parameter min: CGFloat
   - parameter max: CGFloat
   
   - returns: CGFloat random number
   */
  public static func random(_ min: CGFloat, max: CGFloat) -> CGFloat {
    return CGFloat.random * (max - min) + min
  }
}
