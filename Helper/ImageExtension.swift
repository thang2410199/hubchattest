//
//  ImageExtension.swift
//  
//
//  Created by Ngo Quoc Thang on 8/20/16.
//  Copyright © 2016 . All rights reserved.
//

import UIKit

//public func delay(delay:Double, closure:()->()) {
//  dispatch_after(
//    dispatch_time(
//      DISPATCH_TIME_NOW,
//      Int64(delay * Double(NSEC_PER_SEC))
//    ),
//    dispatch_get_main_queue(), closure)
//}


extension UIImage {
  /**
   Create UIImage from single color
   
   - parameter color: color
   
   - returns: UIImage size 1x1
   */
  public static func fromColor(_ color: UIColor) -> UIImage {
    let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
    UIGraphicsBeginImageContext(rect.size)
    let context = UIGraphicsGetCurrentContext()
    context?.setFillColor(color.cgColor)
    context?.fill(rect)
    let img = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return img!
  }
  /**
   
   Create UIImage from single color
   
   - parameter color: color
   - parameter size: size
   
   - returns: UIImage
   */
  public static func fromColor(_ color: UIColor, size: CGSize) -> UIImage {
    let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
    UIGraphicsBeginImageContext(rect.size)
    let context = UIGraphicsGetCurrentContext()
    context?.setFillColor(color.cgColor)
    context?.fill(rect)
    let img = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return img!
  }
  
  /**
   Create image from Url
   
   - parameter URL: String
   
   - returns: UIImage
   */
  public static func imageFromURL(_ URL: String) -> UIImage {
    let url = Foundation.URL(string: URL)
    let data = try? Data(contentsOf: url!)
    return UIImage(data: data!)!
  }
  
  public func convertImageToBase64() -> String {
    let imageData = UIImageJPEGRepresentation(self, 1)
    let base64Data = imageData!.base64EncodedString(options: [])
    return base64Data
  }
  
  public func resize(percent: CGFloat) -> UIImage? {
    let imageView = UIImageView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: self.size.width * percent, height: self.size.height * percent)))
    imageView.contentMode = .scaleAspectFit
    imageView.image = self
    UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, self.scale)
    guard let context = UIGraphicsGetCurrentContext() else {
      return nil
    }
    imageView.layer.render(in: context)
    guard let result = UIGraphicsGetImageFromCurrentImageContext() else {
      return nil
    }
    
    return result
  }
  
  public func resize(width: CGFloat) -> UIImage? {
    let scaleFactor = width / self.size.width
    
    let imageView = UIImageView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: width, height: self.size.height * scaleFactor)))
    imageView.contentMode = .scaleAspectFit
    imageView.image = self
    UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, self.scale)
    guard let context = UIGraphicsGetCurrentContext() else {
      return nil
    }
    imageView.layer.render(in: context)
    guard let result = UIGraphicsGetImageFromCurrentImageContext() else {
      return nil
    }
    
    return result
  }
  
  public func changeQuality(_ newQuality: CGFloat) -> UIImage? {
    guard let data = UIImageJPEGRepresentation(self, newQuality) else {
      return nil
    }
    
    return UIImage(data: data)
  }
  
  public func scaleImageRetainingFormat(_ scale: CGFloat, withInterpolationQuality quality: CGInterpolationQuality) -> UIImage {
    UIGraphicsBeginImageContextWithOptions(self.size, true, scale)
    let context = UIGraphicsGetCurrentContext()
    context!.interpolationQuality = quality
    self.draw(in: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return newImage!
  }
}
