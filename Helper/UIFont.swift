//
//  UIFont.swift
//  
//
//  Created by Ngo Quoc Thang on 9/9/16.
//  Copyright © 2016 . All rights reserved.
//

import UIKit

enum DynamicType : String {
  case Body = "UIFontTextStyleBody"
  case Headline = "UIFontTextStyleHeadline"
  case Subheadline = "UIFontTextStyleSubheadline"
  case Footnote = "UIFontTextStyleFootnote"
  case Caption1 = "UIFontTextStyleCaption1"
  case Caption2 = "UIFontTextStyleCaption2"
}

/**
 Used for custom font
 
 - Regular:
 - HeavyItalic:
 - Bold:
 - Italic:
 - Small:
 */
public enum FontBook: String {
  case Regular = "Avenir"
  case HeavyItalic = "AvenirNext-HeavyItalic"
  case Bold = "bold"
  case Italic = "italic"
  case Small = "small"
  
  /**
   Return system font for Bold, Italic and Regular
   
   - parameter style: Bold, Italic and Regular, all else will be treated as Regular
   - parameter size:  size of the font
   
   - returns: system font with style and font
   */
  public static func systemFont(_ style: FontBook, size: CGFloat) -> UIFont {
    switch style {
    case .Bold:
      return UIFont.boldSystemFont(ofSize: size)
    case .Italic:
      return UIFont.italicSystemFont(ofSize: size)
    case .Regular:
      return UIFont.systemFont(ofSize: size)
    default:
      return UIFont.systemFont(ofSize: size)
    }
  }
  
  func of(_ style: DynamicType) -> UIFont {
    let preferred = UIFont.preferredFont(forTextStyle: UIFontTextStyle(rawValue: style.rawValue)).pointSize
    return UIFont(name: self.rawValue, size: preferred)!
  }
}
