//
//  StringExtension.swift
//  
//
//  Created by Ngo Quoc Thang on 8/20/16.
//  Copyright © 2016 . All rights reserved.
//

import UIKit

func runOnMainThread(_ block: @escaping () -> Void) {
  DispatchQueue.main.async(execute: {
    block()
  })
}

extension String {  
  
  /// Return the length of the String
  public var length: Int {
    return self.characters.count
  }
  
  var first: String {
    return String(characters.prefix(1))
  }
  var last: String {
    return String(characters.suffix(1))
  }
  var uppercaseFirst: String {
    return first.uppercased() + String(characters.dropFirst())
  }
  
  /**
   Convert String to NSURL
   
   - returns: NSURL
   */
  public func toURL() -> URL? {
    return URL(string: self)
  }
  
  public func htmlToAttributedString(_ text: String) -> NSAttributedString! {
    let htmlData = text.data(using: String.Encoding.utf8, allowLossyConversion: false)
    let htmlString: NSAttributedString?
    do {
      htmlString = try NSAttributedString(data: htmlData!, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
    } catch _ {
      htmlString = nil
    }
    
    return htmlString
  }
  
  subscript (i: Int) -> Character {
    return self[self.characters.index(self.startIndex, offsetBy: i)]
  }
  
  subscript (i: Int) -> String {
    return String(self[i] as Character)
  }
  
  subscript (r: Range<Int>) -> String {
    let start = characters.index(startIndex, offsetBy: r.lowerBound)
    let end = characters.index(start, offsetBy: r.upperBound - r.lowerBound)
    return self[Range(start ..< end)]
  }
  
  func contains(_ s: String) -> Bool
  {
    return self.contains(s) ? true : false
  }
  
  func replace(_ target: String, withString: String) -> String
  {
    return self.replacingOccurrences(of: target, with: withString, options: NSString.CompareOptions.literal, range: nil)
  }
  
  func subString(_ startIndex: Int, length: Int) -> String
  {
    let start = self.characters.index(self.startIndex, offsetBy: startIndex)
    let end = self.characters.index(self.startIndex, offsetBy: startIndex + length)
    return self.substring(with: Range<String.Index>(start..<end))
  }
  
  func indexOf(_ target: String) -> Int
  {
    let range = self.range(of: target)
    if let range = range {
      return self.characters.distance(from: self.startIndex, to: range.lowerBound)
    } else {
      return -1
    }
  }
  
  func indexOf(_ target: String, startIndex: Int) -> Int
  {
    let startRange = self.characters.index(self.startIndex, offsetBy: startIndex)
    
    let range = self.range(of: target, options: NSString.CompareOptions.literal, range: Range<String.Index>(startRange ..< self.endIndex))
    
    if let range = range {
      return self.characters.distance(from: self.startIndex, to: range.lowerBound)
    } else {
      return -1
    }
  }
  
  func lastIndexOf(_ target: String) -> Int
  {
    var index = -1
    var stepIndex = self.indexOf(target)
    while stepIndex > -1
    {
      index = stepIndex
      if stepIndex + target.length < self.length {
        stepIndex = indexOf(target, startIndex: stepIndex + target.length)
      } else {
        stepIndex = -1
      }
    }
    return index
  }
  
  func isMatch(_ regex: String, options: NSRegularExpression.Options) -> Bool
  {
    do {
      let exp = try NSRegularExpression(pattern: regex, options: options)
      let matchCount = exp.numberOfMatches(in: self, options: .anchored, range: NSMakeRange(0, self.length))
      return matchCount > 0
    }
    catch let error as NSError {
      print(error.description)
    }
    return false
  }
  
  func getMatches(_ regex: String, options: NSRegularExpression.Options) -> [NSTextCheckingResult]
  {
    do {
      let exp = try NSRegularExpression(pattern: regex, options: options)
      let matches = exp.matches(in: self, options: .anchored, range: NSMakeRange(0, self.length))
      return matches as [NSTextCheckingResult]
    }
    catch let error as NSError {
      print(error.description)
    }
    return []
  }
  
  fileprivate var vowels: [String]
    {
    get
    {
      return ["a", "e", "i", "o", "u"]
    }
  }
  
  fileprivate var consonants: [String]
    {
    get
    {
      return ["b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w", "x", "z"]
    }
  }
  
  func pluralize(_ count: Int) -> String
  {
    if count == 1 {
      return self
    } else {
      let lastChar = self.subString(self.length - 1, length: 1)
      let secondToLastChar = self.subString(self.length - 2, length: 1)
      var prefix = "", suffix = ""
      
      if lastChar.lowercased() == "y" && vowels.filter({x in x == secondToLastChar}).count == 0 {
        prefix = self[0..<self.length - 1]
        suffix = "ies"
      } else if lastChar.lowercased() == "s" || (lastChar.lowercased() == "o" && consonants.filter({x in x == secondToLastChar}).count > 0) {
        prefix = self[0..<self.length]
        suffix = "es"
      } else {
        prefix = self[0..<self.length]
        suffix = "s"
      }
      
      return prefix + (lastChar != lastChar.uppercased() ? suffix : suffix.uppercased())
    }
  }
  
  func jsonToDictionary() -> [String:AnyObject]? {
    if let data = self.data(using: String.Encoding.utf8) {
      do {
        return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
      } catch let error as NSError {
        print(error)
      }
    }
    return nil
  }
  
  func encodeUTF8() -> String {
    let str = String(validatingUTF8: self.cString(using: String.Encoding.utf8)!)
    return str!
  }
  
  func toCardNumber() -> String {
    return "•••• •••• •••• " + self
  }
}

extension Character {
  
  public func toString() -> String {
    return String(self)
  }
}
