//
//  NSDateExtension.swift
//  
//
//  Created by Ngo Quoc Thang on 9/4/16.
//  Copyright © 2016 . All rights reserved.
//

import Foundation

extension Date {
  
  /**
   convert NSDate to String, see more about format here: http://www.codingexplorer.com/swiftly-getting-human-readable-date-nsdateformatter/
   
   - parameter dateStyle:
   - parameter timeStyle:
   
   - returns: string value of the date
   */
  func toString(_ format: String) -> String {
    let dayTimePeriodFormatter = DateFormatter()
    dayTimePeriodFormatter.dateFormat = format
    
    return dayTimePeriodFormatter.string(from: self)
  }
  
  /**
   convert NSDate to String, see more about format here: http://www.codingexplorer.com/swiftly-getting-human-readable-date-nsdateformatter/
   
   - parameter dateStyle:
   - parameter timeStyle:
   
   - returns: string value of the date
   */
  public func toString(_ dateStyle: DateFormatter.Style, timeStyle: DateFormatter.Style = .short) -> String {
    let dayTimePeriodFormatter = DateFormatter()
    dayTimePeriodFormatter.dateStyle = dateStyle
    dayTimePeriodFormatter.timeStyle = timeStyle
    
    return dayTimePeriodFormatter.string(from: self)
  }
  
  /**
   Convert string to nsdate using specific format at http://userguide.icu-project.org/formatparse/datetime
   
   - parameter value:  value
   - parameter format: format
   
   - return: NSDate?
   */
  public static func fromString(_ value: String?, format: String) -> Date? {
    guard value != nil else {
      return nil
    }
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = format
    return dateFormatter.date(from: value!)
  }
  
  /**
   Return age number from NSDate birth date.
   
   - returns: Int
   */
  public func toAgeByYear() -> Int {
    var age: Int {
      return (Calendar.current as NSCalendar).components(.year, from: self, to: Date(), options: []).year!
    }
    return age
  }
  
  public func toAgeByMonth() -> Int {
    let ageYear = (Calendar.current as NSCalendar).components(.year, from: self, to: Date(), options: []).year
    let ageMonth = (Calendar.current as NSCalendar).components(.month, from: self, to: Date(), options: []).month
    return (ageYear! * 12) + ageMonth!
  }
}
