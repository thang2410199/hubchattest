//
//  UIView.swift
//  
//
//  Created by Ngo Quoc Thang on 9/11/16.
//  Copyright © 2016 . All rights reserved.
//

import UIKit
import pop

extension UIView {
  func stopAnimation() {
    self.layer.removeAllAnimations()
  }
  
  func setCornerRadius(_ radius: CGFloat) {
    self.layer.cornerRadius = radius
    self.layer.masksToBounds = true
    self.layer.shouldRasterize = true
    self.layer.rasterizationScale = UIScreen.main.scale
  }
  
  func popAnimate() {
    self.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
    let animation = POPSpringAnimation(propertyNamed: kPOPLayerScaleXY)
    animation?.toValue = NSValue(cgSize: CGSize(width: 1.0, height: 1.0))
    animation?.springSpeed = 7.0
    animation?.springBounciness = 5.0
    self.layer.pop_add(animation, forKey: "popScale")
  }
}
