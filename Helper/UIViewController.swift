//
//  ViewControllerExtension.swift
//
//
//  Created by Ngo Quoc Thang on 8/22/16.
//  Copyright © 2016. All rights reserved.
//

import UIKit
import JDStatusBarNotification

extension UIViewController {
  
  func showAlert(_ title: String?, message: String?, complete: (() -> Void)?) {
    let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let cancelAction = UIAlertAction(title: "OK", style: .cancel) { (action) in
      complete?()
    }
    alertVC.addAction(cancelAction)
    self.present(alertVC, animated: true) {
      
    }
  }
  
  func showStatusBarNotification(_ message: String, forSecond: Double, style: JDStatusBarStyle = JDStatusBarStyle.Default, withActivity: Bool = false) {
    JDStatusBarNotification.show(withStatus: message, dismissAfter: forSecond, styleName: style.rawValue)
    if(withActivity) {
      JDStatusBarNotification.showActivityIndicator(true, indicatorStyle: .gray)
    }
  }
  
  func setStatusBarColor(_ color: UIColor) {
    let statWindow = UIApplication.shared.value(forKey: "statusBarWindow") as! UIView
    if let statusBar = statWindow.subviews.first {
      statusBar.backgroundColor = color
    }
  }
}

enum JDStatusBarStyle: String {
  case Error = "JDStatusBarStyleError"
  case Warning = "JDStatusBarStyleWarning"
  case Success = "JDStatusBarStyleSuccess"
  case Matrix = "JDStatusBarStyleMatrix"
  case Default = "JDStatusBarStyleDefault"
  case Dark = "JDStatusBarStyleDark"
}
