//
//  NSData.swift
//  
//
//  Created by Ngo Quoc Thang on 10/29/16.
//  Copyright © 2016 . All rights reserved.
//

import Foundation

extension Data {
  func getDeviceToken() -> String {
    let characterSet: CharacterSet = CharacterSet(charactersIn: "<>")
    
    let deviceTokenString: String = (self.description as NSString)
      .trimmingCharacters(in: characterSet)
      .replacingOccurrences(of: " ", with: "") as String
    return deviceTokenString
  }
  
  func jsonToDictionary() -> [String:AnyObject]? {
    if let data = String(data: self, encoding: String.Encoding.utf8)!.data(using: String.Encoding.utf8) {
      do {
        return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
      } catch let error as NSError {
        print(error)
      }
    }
    return nil
  }
}
