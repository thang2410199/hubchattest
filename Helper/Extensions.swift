//
//  Extensions.swift
//  
//
//  Created by Ngo Quoc Thang on 8/5/16.
//  Copyright © 2016 . All rights reserved.
//

import Foundation
import UIKit

extension Int {
  func toString()-> String{
    return String(self)
  }
  
  func isSuccess() -> Bool {
    if(self >= 200 && self <= 399) {
      return true
    }
    return false
  }
  
  func toDouble() -> Double {
    return Double(self)
  }
  
  func toCGFloat() -> CGFloat {
    return CGFloat(self)
  }
}

extension CGFloat {
  func toInt() -> Int {
    return Int(self)
  }
}

extension Float {
  func toString()-> String{
    return "\(self)"
  }
  
  var cleanValue: String {
    return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
  }
  
  func toDouble() -> Double{
    return Double(self)
  }
}
extension Double {
  func toString()-> String{
    return "\(self)"
  }
  
  var cleanValue: String {
    return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
  }
  
  func toStringWithEuros() -> String {
    return "\(self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self))" + "€"
  }
  
  func toFloat() -> Float{
    return Float(self)
  }
}
extension NSObject {
  func toString()-> String{
    return "\(self)"
  }
}

extension NSString {
  override func toString() -> String {
    return String(self)
  }
}

extension Bool {
  func toString() -> String {
    return String(self)
  }
}
